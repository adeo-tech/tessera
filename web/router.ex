defmodule Tessera.Router do
  use Tessera.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Tessera.Auth, repo: Tessera.Repo
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Tessera do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    get "/logout", SessionController, :delete
    get "/login", SessionController, :new
    get "/signup", UserController, :new

    resources "/users", UserController
    resources "/tickets", TicketController 
    resources "/projects", ProjectController
    resources "/sessions", SessionController, only: [:new, :create, :delete]
  end

  # Other scopes may use custom stacks.
  # scope "/api", Tessera do
  #   pipe_through :api
  # end
end
