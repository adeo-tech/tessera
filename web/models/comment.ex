defmodule Tessera.Comment do
  use Tessera.Web, :model

  schema "comments" do
    field :body, :string
    belongs_to :author, Tessera.Author
    belongs_to :ticket, Tessera.Ticket

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:body])
    |> validate_required([:body])
  end
end
