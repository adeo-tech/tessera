defmodule Tessera.Ticket do
  use Tessera.Web, :model

  schema "tickets" do
    field :ticket_key, :string
    field :summary, :string
    field :description, :string
    belongs_to :assignee, Tessera.Assignee
    belongs_to :reporter, Tessera.Reporter
    belongs_to :project, Tessera.Project

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:ticket_key, :summary, :description])
    |> validate_required([:ticket_key, :summary, :description])
    |> unique_constraint(:ticket_key)
  end
end
