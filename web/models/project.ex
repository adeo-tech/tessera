defmodule Tessera.Project do
  use Tessera.Web, :model

  schema "projects" do
    field :pkey, :string
    field :name, :string
    field :repo, :string
    field :homepage, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:pkey, :name, :repo, :homepage])
    |> validate_required([:pkey, :name, :repo, :homepage])
    |> unique_constraint(:pkey)
    |> unique_constraint(:name)
  end
end
