defmodule Tessera.Status do
  use Tessera.Web, :model

  schema "statuses" do
    field :name, :string
    field :status_type, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :status_type])
    |> validate_required([:name, :status_type])
    |> unique_constraint(:name)
  end
end
