defmodule Tessera.User do
  use Tessera.Web, :model

  import Comeonin.Bcrypt, only: [hashpwsalt: 1]

  @required_fields ~w(username email password full_name)
  @optional_fields ~w(is_admin)

  schema "users" do
    field :username, :string
    field :email, :string
    field :full_name, :string
    field :password, :string, virtual: true
    field :password_digest, :string
    field :is_admin, :boolean, default: false

    timestamps()
  end

  def generate_password(changeset) do
    put_change(changeset, 
     :password_digest, 
     hashpwsalt(changeset.params["password"]))
  end

  def generate_gravatar(changeset) do
    put_change(changeset,
      :profile_pic,
      :crypto.hash(:md5, changeset.params["email"]) |> Base.encode16())
  end


  def registration_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields, @optional_fields)
    |> validate_required([:username, :email, :full_name, :password])
    |> unique_constraint(:username)
    |> validate_format(:email, ~r/@/)
    |> validate_length(:password, min: 8)
    |> generate_password
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields, @optional_fields)
    |> validate_required([:username, :email, :full_name, :password])
  end
end
