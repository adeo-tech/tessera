defmodule Tessera.SessionController do
  use Tessera.Web, :controller

  plug :put_layout, "login_signup_layout.html"

  def new(conn, _params) do
    render conn, "new.html"
  end

  def create(conn, 
   %{"session" => %{ "username" => username, "password" => password}}) do
    case Tessera.Auth.login_by_username_and_pass(conn, username, password, repo: Repo) do
      {:ok, conn} ->
        conn
        |> put_flash(:info, "Successfully logged in.")
        |> redirect(to: "/")
      {:error, _reason, conn} ->
        conn
        |> put_flash(:error, "Wrong username or password.")
        |> render("new.html")
    end
   end

   def delete(conn, _opts) do
     conn
     |> Tessera.Auth.logout
     |> put_flash(:info, "Successfully logged out.")
     |> redirect(to: page_path(conn, :index))
   end
end
