defmodule Tessera.PageController do
  use Tessera.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
