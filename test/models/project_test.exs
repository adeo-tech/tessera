defmodule Tessera.ProjectTest do
  use Tessera.ModelCase

  alias Tessera.Project

  @valid_attrs %{homepage: "some content", name: "some content", pkey: "some content", repo: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Project.changeset(%Project{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Project.changeset(%Project{}, @invalid_attrs)
    refute changeset.valid?
  end
end
