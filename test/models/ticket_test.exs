defmodule Tessera.TicketTest do
  use Tessera.ModelCase

  alias Tessera.Ticket

  @valid_attrs %{description: "some content", summary: "some content", ticket_key: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Ticket.changeset(%Ticket{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Ticket.changeset(%Ticket{}, @invalid_attrs)
    refute changeset.valid?
  end
end
