defmodule Tessera.TicketTypeTest do
  use Tessera.ModelCase

  alias Tessera.TicketType

  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = TicketType.changeset(%TicketType{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = TicketType.changeset(%TicketType{}, @invalid_attrs)
    refute changeset.valid?
  end
end
