defmodule Tessera.Repo.Migrations.CreateTicket do
  use Ecto.Migration

  def change do
    create table(:tickets) do
      add :ticket_key, :string
      add :summary, :string
      add :description, :text
      add :assignee_id, references(:users, on_delete: :nothing)
      add :reporter_id, references(:users, on_delete: :nothing)
      add :project_id, references(:projects, on_delete: :nothing)

      timestamps()
    end
    create unique_index(:tickets, [:ticket_key])
    create index(:tickets, [:assignee_id])
    create index(:tickets, [:reporter_id])
    create index(:tickets, [:project_id])

  end
end
