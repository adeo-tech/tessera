defmodule Tessera.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :username, :string
      add :email, :string
      add :full_name, :string
      add :password_digest, :string
      add :is_admin, :boolean, default: false, null: false

      timestamps()
    end
    create unique_index(:users, [:username])

  end
end
