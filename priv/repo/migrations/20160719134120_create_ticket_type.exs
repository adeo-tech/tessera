defmodule Tessera.Repo.Migrations.CreateTicketType do
  use Ecto.Migration

  def change do
    create table(:ticket_types) do
      add :name, :string

      timestamps()
    end
    create unique_index(:ticket_types, [:name])

  end
end
