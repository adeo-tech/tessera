defmodule Tessera.Repo.Migrations.CreateProject do
  use Ecto.Migration

  def change do
    create table(:projects) do
      add :pkey, :string
      add :name, :string
      add :repo, :string
      add :homepage, :string

      timestamps()
    end
    create unique_index(:projects, [:pkey])
    create unique_index(:projects, [:name])

  end
end
