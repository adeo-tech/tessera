defmodule Tessera.Repo.Migrations.CreateStatus do
  use Ecto.Migration

  def change do
    create table(:statuses) do
      add :name, :string
      add :status_type, :integer

      timestamps()
    end
    create unique_index(:statuses, [:name])

  end
end
